from plone.indexer.decorator import indexer
from Products.Ploneboard.interfaces import IComment
from Products.Ploneboard.interfaces import IConversation
from Products.Ploneboard.interfaces import IForum

from communities.practice.indexers import copLastModification
from communities.practice.indexers import subCoPDepth
from communities.practice.metadata import partOfCommunity

@indexer(IConversation)
def num_comments(obj):
    return obj.getNumberOfComments()

@indexer(IConversation)
def copLastModificationConversation(obj):
    return copLastModification(obj)

@indexer(IComment)
def partOfCommunityComment(obj):
    return partOfCommunity(obj)

@indexer(IConversation)
def partOfCommunityConversation(obj):
    return partOfCommunity(obj)

@indexer(IComment)
def subCoPDepthComment(obj):
    return subCoPDepth(obj)

@indexer(IConversation)
def subCoPDepthConversation(obj):
    return subCoPDepth(obj)

@indexer(IForum)
def subCoPDepthForum(obj):
    return subCoPDepth(obj)
